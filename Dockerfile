# get gitversion info
FROM gittools/gitversion:5.1.3-beta1-38-linux-ubuntu-16.04-netcoreapp2.1 AS gitversion
COPY ./.git /repo/
COPY ./GitVersion.yml /repo/
RUN dotnet /app/GitVersion.dll /repo > /version.json

FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY src/*.sln .
COPY src/WebApi/*.csproj ./WebApi/
COPY src/Tests.Unit/*.csproj ./Tests.Unit/
COPY src/Tests.Contract.Service/*.csproj ./Tests.Contract.Service/
COPY src/Tests.Contract.Consumer/*.csproj ./Tests.Contract.Consumer/
RUN dotnet restore

# copy everything else and build app
COPY src/WebApi/. ./WebApi/
COPY src/Tests.Unit/. ./Tests.Unit/
COPY src/*.ruleset ./
COPY --from=gitversion /version.json ./WebApi/

# run unit tests
WORKDIR /app/Tests.Unit
RUN dotnet test

# publish stuff
WORKDIR /app/WebApi
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 AS runtime
WORKDIR /app
COPY --from=build /app/WebApi/out ./
ENTRYPOINT ["dotnet", "WebApi.dll"]
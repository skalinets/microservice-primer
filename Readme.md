# Microservice Primer
This project contains the C# microservice template demoing different aspects and tools.

## Docker Support

There is a `Dockerfile` that builds the service. It updates version information, run tests. 

To build a docker image, run `./build-docker.sh`. It uses [gitversion][1] to extract the version information and tag resulting docker image accordingly.

To run image in docker, us this command: 

```bash
docker run -it --rm -p 3000:80 hof/some-service:latest
```

## Versioning

Project uses [gitversion][1] to automatally get the SemVer version from the git history. 

Running service exposes `/version` endpoint that returns it's version.

## Unit Tests

There is a test project at `/src/Test.Unit` with unit tests. These tests are executed during docker build.

## Contract Tests

There are contract tests at `src/Tests.Contract.*`. [Pact .NET][2] is used for contract tests. These tests **ARE NOT** included into CI/CD pipeline and for now should be run manually. First consumer test are executed, they generate pact files and then service tests use these pact files to verify the service behavior. More on that matter is available [here][3].

## Logging

Project has Serilog logging, see [this blog post][4] for more details.

## Kafka Support

Project uses `Confluent.Kafka` nuget to integrate with Kafka. All configuration is done via `appsettings.json` and could be overwritten by [environment variables, command line arguments and so on][5].

## Static Code Analysis

Project contains static code analysis built in. It uses [Roslyn analyzers][6] that run during project build. Also project has *treat warnings as errors* setting on, so if there are build warnings, build fails. 

Rules for analyzers are in the file `./src/Service.rulest`. 

[1]: https://gitversion.readthedocs.io/
[2]: https://github.com/pact-foundation/pact-net
[3]: https://github.com/tdshipley/pact-workshop-dotnet-core-v1
[4]: https://nblumhardt.com/2019/10/serilog-in-aspnetcore-3/
[5]: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.0
[6]: https://github.com/dotnet/roslyn-analyzers
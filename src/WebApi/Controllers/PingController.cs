using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PingController : ControllerBase
    {
        private readonly IMediator mediator;
        private readonly IMapper mapper;

        public PingController(IMediator mediator, IMapper mapper)
        {
            this.mediator = mediator;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<PingResponse> Post(PingRequest request)
        {
            var task = await mediator.Send(mapper.Map<PingRequest, PingService.Request>(request));
            return mapper.Map<PingService.Result, PingResponse>(task);
        }

        public class PingResponse
        {
            public TimeSpan Time { get; set; }

            public string Message { get; set; }

            public class MappingProfile : Profile
            {
                public MappingProfile() => CreateMap<PingService.Result, PingResponse>();
            }
        }

        public class PingRequest
        {
            public string Message { get; set; }

            public class MappingProfile : Profile
            {
                public MappingProfile() => CreateMap<PingRequest, PingService.Request>();
            }
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VersionController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public VersionController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpGet]
        public object Get()
        {
            return new
            {
                version = configuration["FullSemVer"],
                informationVersion = configuration["InformationalVersion"],
            };
        }
    }
}

using System;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WebApi.Controllers
{
    public static class PingService
    {
        public class Request : IRequest<Result>
        {
            public string Message { get; set; }
        }

        public class Handler : IRequestHandler<Request, Result>
        {
            private readonly KafkaConfig kafkaConfig;
            private readonly ILogger<Handler> logger;

            public Handler(KafkaConfig kafkaConfig, ILogger<Handler> logger)
            {
                this.kafkaConfig = kafkaConfig;
                this.logger = logger;
            }

            public async Task<Result> Handle(Request request, CancellationToken cancellationToken)
            {
                using var producer = new ProducerBuilder<string, string>(kafkaConfig.Producer).Build();
                var dateTime = DateTime.Now;

                await producer.ProduceAsync(kafkaConfig.Topic, new Message<string, string>
                {
                    Key = Guid.NewGuid().ToString(),
                    Value = JsonConvert.SerializeObject(new { request.Message }),
                });
                return new Result
                {
                    Message = request.Message,
                    Time = DateTime.Now.Subtract(dateTime),
                };
            }
        }

        public class Result
        {
            public string Message { get; set; }

            public TimeSpan Time { get; set; }
        }
    }
}

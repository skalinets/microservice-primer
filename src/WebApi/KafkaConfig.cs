using Confluent.Kafka;

namespace WebApi
{
    public class KafkaConfig
    {
        public string Topic { get; set; }

        public ConsumerConfig Consumer { get; set; }

        public ProducerConfig Producer { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Newtonsoft.Json;

namespace tests.Middleware
{
    public class ProviderStateMiddleware
    {
        private const string ConsumerName = "Consumer";
        private readonly RequestDelegate next;
        private readonly IDictionary<string, Action> providerStates;

        public ProviderStateMiddleware(RequestDelegate next)
        {
            this.next = next;
            providerStates = new Dictionary<string, Action>
            {
                {
                    "default state",
                    DoNothing
                }
            };
        }

        private void DoNothing()
        {
            
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value == "/provider-states")
            {
                await this.HandleProviderStatesRequest(context);
                await context.Response.WriteAsync(String.Empty);
            }
            else
            {
                await this.next(context);
            }
        }

        private async Task HandleProviderStatesRequest(HttpContext context)
        {
            context.Response.StatusCode = (int)HttpStatusCode.OK;

            var request = context.Request;
            var body = request.Body;
            if (string.Equals(request.Method, nameof(HttpMethod.Post), StringComparison.OrdinalIgnoreCase) &&
                body != null)
            {
                string jsonRequestBody = String.Empty;
                using (var reader = new StreamReader(body, Encoding.UTF8))
                {
                    jsonRequestBody = await reader.ReadToEndAsync();
                }

                var providerState = JsonConvert.DeserializeObject<ProviderState>(jsonRequestBody);

                //A null or empty provider state key must be handled
                if (providerState != null && !String.IsNullOrEmpty(providerState.State) &&
                    providerState.Consumer == ConsumerName)
                {
                    providerStates[providerState.State].Invoke();
                }
            }
        }
    }
    public class ProviderState
    {
        public string Consumer { get; set; }
        public string State { get; set; }
    }
}
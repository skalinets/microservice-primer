using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using PactNet.Matchers;
using PactNet.Mocks.MockHttpService;
using PactNet.Mocks.MockHttpService.Models;
using Xunit;

namespace Tests.Contract.Consumer
{
    public class UnitTest1: IClassFixture<ServiceApiPact>
    {
        private readonly IMockProviderService _mockProviderService;
        private readonly string _mockProviderServiceBaseUri;

        public UnitTest1(ServiceApiPact data)
        {
            _mockProviderService = data.MockProviderService;
            _mockProviderServiceBaseUri = data.MockProviderServiceBaseUri;
            _mockProviderService.ClearInteractions();
        }

        [Fact]
        public async Task this_is_just_a_test()
        {
            //Arrange
            _mockProviderService.Given("default state")
                .UponReceiving("a request to retrieve a version")
                .With(new ProviderServiceRequest
                {
                    Method = HttpVerb.Get,
                    Path = "/version",
                    Headers = new Dictionary<string, object>
                    {
                        { "Accept", "application/json" },
                    }
                })
                .WillRespondWith(new ProviderServiceResponse
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8" }
                    },
                    Body = new 
                    {
                        version = Match.Type(""),
                        informationVersion = Match.Type("")
                    }
                });

            var consumer = new HttpClient();
            consumer.DefaultRequestHeaders.Add("Accept", "application/json");
            await consumer.GetAsync($"{_mockProviderServiceBaseUri}/version");

            _mockProviderService.VerifyInteractions();
        }
    }
}

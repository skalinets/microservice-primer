SEM_VER=$(docker run --rm -v "$(pwd):/repo" --log-driver=none -a stdin -a stdout -a stderr \
  gittools/gitversion:5.1.3-beta1-38-linux-ubuntu-16.04-netcoreapp2.1 /repo /showvariable FullSemVer ) 

echo $SEM_VER
docker build . -t hof/some-service:$SEM_VER -t hof/some-service:latest